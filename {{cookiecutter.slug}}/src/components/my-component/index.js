import {LitElement, html{% if cookiecutter.css_modules.lower() == "n" -%}, css{%- endif %}, property, customElement} from 'lit-element';
{% if cookiecutter.css_modules.lower() == "y" -%}
import styles from './styles.css';

export class LightElement extends LitElement {
    createRenderRoot() {
        // Skip Shadow DOM Generation Shadow DOM
        return this;
    }

    $(selector) {
        return this.querySelector(selector);
    }
}

export class ShadowElement extends LitElement {
    $(selector) {
        return this.shadowRoot.querySelector(selector);
    }
}
{%- endif %}

@customElement('my-component')
export class MyComponent extends {% if cookiecutter.css_modules.lower() == "y" %}LightElement{% else %}LitElement{% endif %} {
    @property({type: String}) name = '';
{% if cookiecutter.css_modules.lower() == "n" -%}

    static styles = css`
        .text {
            color: blue;
        }
    `;

{%- endif %}
    render() {
        return html`
            <h1 class="{% if cookiecutter.css_modules.lower() == "y" -%}${styles.text}{%- else -%}text{%- endif %}">${this.name}</h1>
        `;
    }
}
