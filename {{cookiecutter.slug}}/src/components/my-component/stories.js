import {storiesOf} from '@storybook/polymer';
import {text} from '@storybook/addon-knobs';
import {html} from 'lit-html';

import './index';

storiesOf('my-component', module)
    .add('standard', () => html`
        <my-component name="${text('name', 'Hello World')}"/>
    `);
