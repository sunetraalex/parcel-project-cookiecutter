{{cookiecutter.name}}
======================

{{cookiecutter.description}}

## Getting started

```sh
npm install
npm start
```

Then the project is available at `http://localhost:3000/`.

## Storybook

```sh
npm run storybook
```

Then the project is available at `http://localhost:6006/`.


## Build

```sh
npm run build
```

Then you can find the compiled project under `public/`.
