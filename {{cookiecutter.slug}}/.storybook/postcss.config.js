module.exports = {
    "plugins": {
        "postcss-prepend-imports": {
            "path": "src",
            "files": [
                "_global.css"
            ]
        },
        "postcss-import": {},
        "autoprefixer": {
            "grid": true
        },
        "postcss-unit-to-unit": {
            "from": "x",
            "multiple": 8
        },
        "postcss-short": {},
        "postcss-arithmetic": {
            "calcMultipleUnits": false
        },
        "level4": {
            "preserve": true
        },
        "postcss-calc": {},
        "postcss-discard-duplicates": {},
        "cssnano": {
            "preset": "default"
        },
        "postcss-reporter": {
            "clearReportedMessages": true
        }
    }
}
