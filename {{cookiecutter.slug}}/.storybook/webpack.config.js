const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
            },
          },
          {
              loader: 'postcss-loader',
              options: {
                  config: {
                      path: path.join(__dirname, 'postcss.config.js')
                  }
              }
          },
        ],
      },
    ],
  },
}
