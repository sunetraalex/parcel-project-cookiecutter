#!/usr/bin/env sh

# Delete Storybook folder if not used
{% if cookiecutter.storybook.lower() == "n" %}
rm -rf .storybook/
{% endif %}

# Delete Web Components folder if not used
{% if cookiecutter.web_components.lower() == "n" %}
rm -rf src/components/
{% endif %}
